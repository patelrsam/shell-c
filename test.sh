# Author Shubham Patel (Sam)
# Year 2015

# MAKE SURE YOU DONOT HAVE A "test" FOLDER!!!!!!!!!

rm -rf run 

echo "Enter name of the executable file(most likly a.out): "
read -e name
echo "Enter number of public tests .in files available for the project: "
read -e num

mkdir run
i=0
name1=public0
name2=out
while [ $i -le $num ] 
do
	name3=${i}
	name4=${i}
	./$name < $PWD/testing/${name1}${name3}.in >  $PWD/run/${name4}_${name2}.output
	((i++))
done
