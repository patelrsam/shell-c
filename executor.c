#include <stdio.h>
#include "command.h"
#include "executor.h"

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sysexits.h>
#include <fcntl.h>


#define CORRECT 0
#define FAIL 1

#define PIPE_LEFT 100
#define PIPE_RIGHT 200
#define PIPE_NONE -1



/* Auxiliary method with parent input and output file descriptors
   pip_indi is a indicator for if the method was called by a pip parent,
   if it was, it tell whether it was left or right
   additionally it  */
static int exe_aux(struct tree *t, int p_in, int p_out, int pipe_indi,
                   int pipe_array[]) {

   int status;

   fflush(stdout);
   /*NODE node*/
   if (t->conjunction == NONE) {

      if (strcmp(t->argv[0], "exit") == 0) {
         exit(CORRECT);
      }
      else if (strcmp(t->argv[0], "cd") == 0) {

         if (t->argv[1]) {
            chdir(t->argv[1]);
         } else {
            chdir(getenv("HOME"));
         }
         return CORRECT;

      }
      else {

         pid_t child;

         printf("Arg: %s Input: %s Output: %s \n",t->argv[0],t->input,t->output);

         if ((child = fork()) < 0) {
            perror("Fork");
         }
         /*parent block*/
         else if (child > 0) {
            /*if pip was used by the parent, respective files are closed*/
            if (pipe_indi == PIPE_LEFT) {
               close(pipe_array[1]);
            }
            else if (pipe_indi == PIPE_LEFT) {
               close(pipe_array[0]);
            }
            /*Reaping*/
            wait(&status);
            return status;
         }
         else {
            /*Child block*/
            int file_in, file_out;
            /*if pip was used by the parent, respective files are closed*/
            if (pipe_indi == PIPE_RIGHT) {
               close(pipe_array[1]);
            }
            if (pipe_indi == PIPE_LEFT) {
               close(pipe_array[0]);
            }

            /*input and output are adjusted*/
            if (t->input) {
               file_in = open(t->input, O_RDONLY);
               if (file_in == -1) {
                  perror("Open Failed-Input");
                  exit(FAIL);
               }

               if (dup2(file_in, STDIN_FILENO) == -1) {
                  perror("Dup2");
                  close(file_in);
                  exit(FAIL);
               }

               if (pipe_indi == PIPE_RIGHT) {
                  close(p_in);
               }
            }

            if (t->output) {
               file_out = open(t->output, O_WRONLY | O_CREAT, 0664);
               if (file_out == -1) {
                  perror("Open Failed-Output");
                  exit(1);
               }
               if ((dup2(file_out, STDOUT_FILENO)) == -1) {
                  perror("Dup2");
                  close(p_out);
                  exit(FAIL);
               }

               if (pipe_indi == PIPE_LEFT) {
                  close(p_out);
               }
            }

            if (t->input == NULL) {

               if (dup2(p_in, STDIN_FILENO) == -1) {

                  perror("Dup2");
                  close(p_in);
                  exit(FAIL);
               }
               if (pipe_indi == PIPE_RIGHT) {
                  close(p_in);
               }
            }

            if (t->output == NULL) {

               if (dup2(p_out, STDOUT_FILENO) == -1) {

                  perror("Dup2");
                  close(p_out);
                  exit(FAIL);
               }
               if (pipe_indi == PIPE_LEFT) {
                  close(p_out);
               }
            }
            /*command is executed*/
            execvp(t->argv[0], t->argv);
            printf("Failed to execute %s\n", t->argv[0]);

            fflush(stdout);
            exit(FAIL);
         }

      }


   }

   /*AND block*/
   else if (t->conjunction == AND) {
      /*input and out files are opened*/
      int i;
      if (t->input) {
         p_in = open(t->input, O_RDONLY);
         if (p_in == -1) {
            perror("Open Failed-Input");
            exit(FAIL);
         }
      }
      if (t->output) {
         p_out = open(t->output, O_WRONLY | O_CREAT, 0664);
         if (p_out == -1) {
            perror("Open Failed-Output");
            exit(FAIL);
         }
      }

      /*first the left executes and it the correct exit status is returned
         right executes*/
      i = exe_aux(t->left, p_in, p_out, PIPE_NONE, NULL);

      if (WEXITSTATUS(i) == 0) {
         exe_aux(t->right, p_in, p_out, PIPE_NONE, NULL);
      }

   }

   /*PIPE block*/
   else if (t->conjunction == PIPE) { 

      int fd_rw[2];
      /*Ambiguous redirect*/
      if(t->left->output != NULL){
         perror("Ambiguous output redirect");
         exit(FAIL);
       
      }
      if( t->right->input !=NULL){
         perror("Ambiguous input redirect");
         exit(FAIL);
      }
      /*input and out files are opened*/
      if (t->input) {
         p_in = open(t->input, O_RDONLY);
         if (p_in == -1) {
            perror("Open Failed-Input");
            exit(FAIL);
         }
      }
      if (t->output) {
         p_out = open(t->output, O_WRONLY | O_CREAT, 0664);
         if (p_out == -1) {
            perror("Open Failed-Output");
            exit(FAIL);
         }
      }
      /*pipes are created*/
      if (pipe(fd_rw) < 0) {
         perror("Pipe");
         exit(FAIL);
      }

      /*recursive call is made using respective file descriptors*/
      exe_aux(t->left, p_in, fd_rw[1], PIPE_LEFT, fd_rw);
      exe_aux(t->right, fd_rw[0], p_out, PIPE_RIGHT, fd_rw);

   }

}

/*execute function, calls the auxiliary function*/
int execute(struct tree *t) {
   exe_aux(t, 0, 1, PIPE_NONE, NULL);
   return 0;
}


